class GalleryController extends HTMLElement {

  constructor() {
    super();

    this.locale = document.documentElement.getAttribute('lang') || 'fi-FI';

  }

  connectedCallback() {

    var currentUrl = window.location.href;

    let url = 'https://rustclouflareweb.juhakalastus2626.workers.dev/getimagesrand'


    const ckurl = new URL(currentUrl)

    console.log(ckurl)

    if (ckurl.port === '8080') {

      url = 'http://192.168.1.200:8090/getimagesrand'
    }


    let card = this
    let controller = this

    function GalleryModel() {
      this.load = async id => {
        let res = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'pragma': 'no-cache',
            'X-QUANT': '10'
          },

        })

        let json = await res.json()
        localStorage.setItem('gallery', JSON.stringify(json));

        Object.assign(this, json)
        controller.updateView()
        var $messages = $('.images-content'),
          d, h, m,
          i = 0;

        $messages.mCustomScrollbar();

      }

    }

    let model = new GalleryModel

    //VIEW
    function GalleryView() {

      this.update = (start) => {

        let container = document.createElement('div');
        container.className = 'chat'

        if (start) {

          let image_title = document.createElement('div');
          image_title.id = 'image_title_id'
          image_title.className = 'chat-title'
          let name = document.createElement('h1');
          name.textContent = 'Miia'
          let city = document.createElement('h2');
          city.textContent = 'ilmainen chat';
          let phoneLink = document.createElement("h1");
          phoneLink.textContent = "ilman rekisteröitymistä!"
          let avatar = document.createElement('figure');
          avatar.className = 'avatar'
          let avatarimg = document.createElement('img');
          avatarimg.src = 'https://rustimghoster.markorahat3949.workers.dev/image/27MKwUm4XLjYglkQdzlrghmDGnc.jpg'
          avatar.append(avatarimg)

          let buttonback = document.createElement("button");
          buttonback.className = 'message-submit'
          buttonback.type = 'submit'
          buttonback.type = 'text'
          buttonback.textContent = 'vielä'
          buttonback.onclick = function () { more() }
          image_title.append(name)
          image_title.append(city)
          image_title.append(avatar)
          image_title.append(buttonback)
          container.append(image_title)

        }

        let messages = document.createElement('div');
        messages.id = 'messages_id'
        messages.className = 'messages'

        let messages_content = document.createElement('div');
        messages_content.className = 'images-content'
        messages.append(messages_content)

        for (let i = 0; i < 10; i++) {

          fakeMessage(model[i], false);

        }

        if (start) {
          container.append(messages);
          card.append(container)
        }
      }

    }

    let view = new GalleryView


    window.addEventListener("more", function (e) {

      console.log("more galler")

      var $messages = $('.images-content')
      $messages.mCustomScrollbar("destroy")

      $messages.empty();

      var $messages = $('.images-content'),
        d, h, m,
        i = 0;

      $messages.mCustomScrollbar();

      for (let i = 0; i < 10; i++) {

        fakeMessage(e.detail.more[i], false);

      }

    })


    window.addEventListener("back", function (e) {

     
      let selectedimg = JSON.parse(e.detail.back)

      let image_title = document.getElementById('image_title_id')

      let name = document.createElement('h1');
      name.textContent = selectedimg.name

      image_title.replaceChild(name, image_title.childNodes[0]);

      let city = document.createElement('h2');
      city.textContent = selectedimg.city;
      image_title.replaceChild(city, image_title.childNodes[1]);



      let avatar = document.createElement('figure');
      avatar.className = 'avatar'
      let avatarimg = document.createElement('img');
      avatarimg.src = 'https://rustimghoster.markorahat3949.workers.dev/image/' + selectedimg.id + ''
      avatar.append(avatarimg)
      image_title.replaceChild(avatar, image_title.childNodes[3]);

      var $messages = $('.images-content')
      $messages.mCustomScrollbar("destroy")

      $messages.empty();

      view.update(false)

      var $messages = $('.images-content'),
        d, h, m,
        i = 0;

      $messages.mCustomScrollbar();

    })

    this.init = () => model.load()
    this.updateView = () => view.update(true)
    this.init()

    var $messages = $('.images-content'),
      d, h, m,
      i = 0;

    function updateScrollbar() {
      $messages.mCustomScrollbar("update").mCustomScrollbar('scrollTo', 'bottom', {
        scrollInertia: 10,
        timeout: 0
      });
    }

    function fakeMessage(imgobj, newimg) {


      if (newimg) {

        i = 0
      }

      setTimeout(function () {


        $('<div class="message new"><figure class="avatar"><img src="https://rustimghoster.markorahat3949.workers.dev/image/' + imgobj.id + '" /></figure>' + imgobj.name + ' ' +  imgobj.city + '</div>').appendTo($('.mCSB_container')).addClass('new').click(function () {  

          selectImg(imgobj)

        })
        updateScrollbar();

        i++;
      }, 1000 + (Math.random() * 20) * 100);

    }

  }

}

export { GalleryController }