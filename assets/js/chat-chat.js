class ChatController extends HTMLElement {

    constructor() {
        super();

        this.locale = document.documentElement.getAttribute('lang') || 'fi-FI';


    }

    connectedCallback() {

        var currentUrl = window.location.href;

        let url = '/gochathandlerv5'

        const ckurl = new URL(currentUrl)

        if (ckurl.port === '8080') {
    
          url = 'http://192.168.1.200:8090/gochathandlerv5'
        }
    
        var parts = window.location.hostname.split('.');
        let domain = parts.slice(-2).join('.');

        let chat_card = this
        let controller = this

        function ChatModel() {

            this.load = () => {

                controller.updateView()
            }

        }

        let model = new ChatModel

        window.addEventListener("changeImgEvent", function (e) {

            var $messages = $('.messages-content'),
                d, h, m,
                i = 0;

            $messages.mCustomScrollbar();

            this.lastimg = JSON.parse(e.detail.newimg)

            let chat_title = document.getElementById('chat_title_id')

            let name = document.createElement('h1');
            name.textContent = this.lastimg.name

            chat_title.replaceChild(name, chat_title.childNodes[0]);

            let city = document.createElement('h2');
            city.textContent = this.lastimg.city;
            chat_title.replaceChild(city, chat_title.childNodes[1]);

            let phoneLink = document.createElement("a");
            let phonehref = "tel://" + this.lastimg.phone;
            phoneLink.setAttribute("href", phonehref)
            phoneLink.textContent = this.lastimg.phone;
            chat_title.replaceChild(phoneLink, chat_title.childNodes[2]);

            let avatar = document.createElement('figure');
            avatar.className = 'avatar'
            let avatarimg = document.createElement('img');
            avatarimg.src = 'https://rustimghoster.markorahat3949.workers.dev/image/' + this.lastimg.id + ''
            avatar.append(avatarimg)
            chat_title.replaceChild(avatar, chat_title.childNodes[3]);

            setTimeout(function () {
                fakeMessage(this.lastimg, true);
            }, 100);

        })

        function ChatView() {

            this.update = () => {

                let container = document.createElement('div');
                container.className = 'chat'

                let chat_title = document.createElement('div');
                chat_title.id = 'chat_title_id'
                chat_title.className = 'chat-title'
                let name = document.createElement('h1');
                name.textContent = 'Miia'
                let city = document.createElement('h2');
                city.textContent = 'helsinki';
                let phoneLink = document.createElement("h1");
                phoneLink.textContent = "ilman rekisteröitymistä!"
                let avatar = document.createElement('figure');
                avatar.className = 'avatar'
                let avatarimg = document.createElement('img');
                avatarimg.src = 'https://rustimghoster.markorahat3949.workers.dev/image/27MKwUm4XLjYglkQdzlrghmDGnc.jpg'
                avatar.append(avatarimg)

                let buttonback = document.createElement("button");
                buttonback.className = 'message-submit'
                buttonback.type = 'submit'
                buttonback.type = 'text'
                buttonback.textContent = 'back'
                buttonback.onclick = function () { back() }

                chat_title.append(name)
                chat_title.append(city)
                chat_title.append(phoneLink)
                chat_title.append(avatar)
                chat_title.append(buttonback)

                let messages = document.createElement('div');
                messages.id = 'messages_id'
                messages.className = 'messages'

                let messages_content = document.createElement('div');
                messages_content.className = 'messages-content'
                messages.append(messages_content)

                let message_box = document.createElement('div');
                message_box.className = 'message-box'
                let input = document.createElement("textarea");
                input.className = 'message-input'

                let button = document.createElement("button");
                button.className = 'message-submit'
                button.type = 'submit'
                button.type = 'text'
                button.textContent = 'send'

                message_box.append(input)
                message_box.append(button)

                container.append(chat_title);
                container.append(messages);
                container.append(message_box);

                chat_card.append(container)

            }
        }

        let view = new ChatView

        window.addEventListener("changeImgEvent", function (e) {


            var $messages = $('.messages-content')
            $messages.mCustomScrollbar("destroy")

            $messages.empty();

            var $messages = $('.messages-content'),
                d, h, m,
                i = 0;

            $messages.mCustomScrollbar();


        })

        this.init = () => model.load()
        this.updateView = () => view.update()
        this.init()


        var $messages = $('.messages-content'),
            d, h, m,
            i = 0;

        function updateScrollbar() {
            $messages.mCustomScrollbar("update").mCustomScrollbar('scrollTo', 'bottom', {
                scrollInertia: 10,
                timeout: 0
            });
        }

        function setDate() {
            d = new Date()
            if (m != d.getMinutes()) {
                m = d.getMinutes();
                $('<div class="timestamp">' + d.getHours() + ':' + m + '</div>').appendTo($('.message:last'));
            }
        }

        function insertMessage() {

            // let localstLastinmsg = localStorage.getItem('localstLastinmsg')

            let msg = $('.message-input').val();

            if ($.trim(msg) == '') {
                return false;
            }


            $('<div class="message message-personal">' + msg + '</div>').appendTo($('.mCSB_container')).addClass('new');
            updateScrollbar();
            setTimeout(function () {
                fakeMessage(this.lastimg, false);
            }, 1000 + (Math.random() * 20) * 100);

        }

        $('.message-submit').click(function () {
            insertMessage();
        });

        $(window).on('keydown', function (e) {
            if (e.which == 13) {
                insertMessage();
                return false;
            }
        })

        var Hello = [
            'Moro-moro, miten menee.',
            'Tere, mitä mies.',
            'Heipä-hei kukas sielä?',
            'Heipä hei kovis!',
            'Hei vaan, kuis menee?',
            'Hei, mitäs kuuluu?',
            'Terve, kuinka hurisee?',
            'Hei! Hei!',
            'Hei!',
            'Haloo!']

        function getRandomInt(max) {

            return Math.floor(Math.random() * max);

        }

        function fakeMessage(imgobj, newimg) {

            if (newimg) {

                i = 0
            }

            $('<div class="message loading new"><figure class="avatar"><img src="https://rustimghoster.markorahat3949.workers.dev/image/' + imgobj.id + '" /></figure><span></span></div>').appendTo($('.mCSB_container'));
            updateScrollbar();

            setTimeout(function () {

                if (i > 0) {

                    // console.log(domain)
                    imgobj.domain = domain
                    imgobj.ask = $('.message-input').val()
                    let Nguid = localStorage.getItem('Nguid')
                    imgobj.nguid = Nguid
                    imgobj['chatuuid'] = imgobj['id']
                    let localstLastinmsg = localStorage.getItem('localstLastinmsg')

                    console.log(localstLastinmsg)
                    imgobj.lastinmsg = localstLastinmsg
                    localStorage.setItem('localstLastinmsg', imgobj.ask)


                    $.ajax({
                        url: url,
                        dataType: 'json',
                        type: 'post',
                        contentType: 'application/json',
                        data: JSON.stringify(imgobj),
                        processData: false,
                        success: function (data, textStatus, jQxhr) {
                            $('.message-input').val(null);
                            $('.message.loading').remove();
                            $('<div class="message new"><figure class="avatar"><img src="https://rustimghoster.markorahat3949.workers.dev/image/' + imgobj.id + '" /></figure>' + data.Answer + '</div>').appendTo($('.mCSB_container')).addClass('new')
                            setDate();
                            updateScrollbar();                            
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            console.log(errorThrown);
                        }
                    });
                }

                if (i === 0) {

                    let frand = getRandomInt(Hello.length)
                    $('.message.loading').remove();
                    $('<div class="message new"><figure class="avatar"><img src="https://rustimghoster.markorahat3949.workers.dev/image/' + imgobj.id + '" /></figure>' + Hello[frand] + '</div>').appendTo($('.mCSB_container')).addClass('new');
                    setDate();
                    updateScrollbar();
                }
                i++;
            }, 1000 + (Math.random() * 20) * 100);
        }
    }

}
export { ChatController }