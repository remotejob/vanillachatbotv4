
import {
  UUIDv4, postData
} from "./utils.js";

import {

  ChatController

} from "./chat-chat.js";

import {

  GalleryController

} from "./gallery-gallery.js";


var currentUrl = window.location.href;

let url = '/getimagesrand'


const ckurl = new URL(currentUrl)

if (ckurl.port === '8080') {

  url = 'http://192.168.1.200:8090/getimagesrand'
}


const version = "0.00"

window.onload = function () {

  let localstVersion = localStorage.getItem('localstVersion')

  localStorage.setItem('localstLastinmsg', "")

  if (localstVersion === null) {

    localStorage.clear()

    localStorage.setItem('localstVersion', version)

    let Nguid = UUIDv4.generate();
    localStorage.setItem('Nguid', Nguid)
    

    var item = document.getElementById('chatid');
    item.hidden = true


  } else {

    if (localstVersion !== version) {

      localStorage.clear()
      localStorage.setItem('localstVersion', version)

    }
    let selectedimg = localStorage.getItem('selectedimg')

    if (selectedimg === null) {

      localStorage.setItem('selectedimg', '{"id":""27MKwUm4XLjYglkQdzlrghmDGnc.jpg","name":"Säde","city":"Saunalahti"}')

    }

    var item = document.getElementById('chatid');
    item.hidden = true

  };
}

window.selectImg = function (selectedimg) {

  var item = document.getElementById('galleryid');

  item.hidden = true

  var item = document.getElementById('chatid');
  item.hidden = false

  localStorage.setItem("selectedimg", JSON.stringify(selectedimg))

  const eventDetails = {
    'newimg': JSON.stringify(selectedimg)
  }
  window.dispatchEvent(
    new CustomEvent('changeImgEvent', { 'detail': eventDetails })
  )

}

window.back = function () {

  var item = document.getElementById('galleryid');
  item.hidden = false

  var item = document.getElementById('chatid');
  item.hidden = true

  let selectedimg = localStorage.getItem("selectedimg")
  const eventDetails = {

    'back': selectedimg
  }
  window.dispatchEvent(
    new CustomEvent('back', { 'detail': eventDetails })
  )
}

window.more = function () {


  postData(url, {})
    .then(data => {
      console.log(data); // JSON data parsed by `data.json()` call

      // console.log(data)

      localStorage.setItem('gallery', JSON.stringify(data));

      const eventDetails = {

        'more': data
      }
      window.dispatchEvent(
        new CustomEvent('more', { 'detail': eventDetails })
      )


    });


}

customElements.define('gallery-gallery', GalleryController)
customElements.define('chat-chat', ChatController)